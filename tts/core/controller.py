#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

import datetime as _datetime

from PyQt5.QtCore import (
	pyqtSlot as _slot,
	pyqtSignal as _signal
)

from PyQt5 import (
	Qt as _Qt
)

from tts.core import redux as _redux

_empty_line = ' ' * 16


def _format_display_text(buf, row, col, text):
	# ensure we are writing text within the bounds of the buffer
	if row < 0 or col < 0 or row > 1 or col > 15:
		return buf

	# TODO we split and join the buffer on every change;
	# is it better to keep the buffer in the split format ?
	br = [c for c in buf[row]]
	for i in range(len(text)):
		try:
			br[i +  col] = text[i]
		except IndexError:
			break

	buf[row] = ''.join(br)
	return buf

def _timedelta_to_hhmmss(delta):
	total_seconds = delta.total_seconds()
	hrs, rem = divmod(total_seconds, 3600.0)
	mns, sec = divmod(rem, 60.0)
	return int(hrs), int(mns), sec





def displayReducer(action, state=None):
	state = state or [
		_empty_line, _empty_line
	]

	if action['type'] == 'SET_DISPLAY_TEXT':
		state = _format_display_text(
			state,
			action['row'], action['col'], action['text']
		)

	return state

def tagsReducer(action, state=None):
	state = state or set()

	if action['type'] == 'CLEAR_TAGS':
		state = set()

	if action['type'] == 'TOGGLE_TAG':
		if action['tag'] in state:
			state.discard(action['tag'])
		else:
			state.add(action['tag'])

	return state

def statusReducer(action, state='normal'):
	if action['type'] == 'SET_STATUS':
		state = action['status']

	return state

def taskReducer(action, state=None):
	if action['type'] == 'SET_TASK':
		state = action['task']

	if action['type'] == 'CLEAR_TASK':
		state = None

	return state


def previousTaskReducer(action, state=None):
	if action['type'] == 'SET_PREVIOUS_TASK':
		state = action['task']

	return state


def appReducer(action, state=None):
	state = state or {}
	return {
		'display': displayReducer(action, state.get('display', None)),
		'tags': tagsReducer(action, state.get('tags', None)),
		'status': statusReducer(action, state.get('status', None)),
		'task': taskReducer(action, state.get('task', None)),
		'previousTask': previousTaskReducer(action, state.get('previousTask', None))
	}


class TTSController(_Qt.QObject):
	""" The default device controller.
	"""

	update_display = _signal(list)
	update_selected_tags = _signal(set)
	action_status = _signal(bool, bool)
	status = _signal(str)

	submit = _signal(
		[_datetime.datetime, list]
	)

	def __init__(self, model, views):
		super(TTSController, self).__init__()
		# the controller has some state; but only the state
		# which is displayed on the UI

		for view in views:
			self.__connect_view(view)

		# internal UI state
		self.__store = _redux.Store(appReducer)
		self.__store.apply_middleware([
			_redux.SagaMiddleware([
				self.saga_task_processing
			]),
			_redux.LoggerMiddleware
		])
		self.__store.subscribe(self.__new_state)


		# connections to the model
		self.__model = model # TODO don't like this
		model.submitResult.connect(self.__on_submit_result)
		self.submit.connect(model.on_submit)

		# set up the timer which refershes
		# the clock and other display info
		self.__clock = _Qt.QTimer(self)
		# not every second, since the UI might be hardware
		# with limited bandwidth
		self.__clock.setInterval(5000)
		self.__clock.timeout.connect(self.__on_clock_timeout)
		self.__clock.start()

		self.__store.dispatch({ 'type': 'STARTUP' })

		# update the display immediately after startup
		self.__on_clock_timeout()

	def saga_task_processing(self):
		yield (
			'STARTUP',
			self.saga_task_processing_STARTUP
		)
		yield (
			'SET_TASK',
			self.saga_task_processing_NEXT_TASK
		)
		yield (
			'CLEAR_TASK',
			self.saga_task_processing_END_TASK
		)

	def saga_task_processing_STARTUP(self, action, store):
		# TODO would rather not call the model diretly, can we use signals ?
		last_task = self.__model.get_last_task()
		if last_task is not None:
			last_task_ts, last_task_tags = last_task
			if 'END' in last_task_tags:
				# if the end of the log is an END, then set previous task
				yield { 'type': 'SET_PREVIOUS_TASK', 'task': last_task }
			else:
				# otherwise we are restarting the app, set current task
				yield { 'type': 'SET_TASK', 'task': [last_task_ts, last_task_tags] }

	def saga_task_processing_NEXT_TASK(self, action, store):
		current_task = store.getState()['task']
		yield { 'type': 'SET_PREVIOUS_TASK', 'task': current_task }

	def saga_task_processing_END_TASK(self, action, store):
		# need to set previous task time to the END time, to display the idle time.
		yield { 'type': 'SET_PREVIOUS_TASK', 'task': [_datetime.datetime.now(), ['END']] }

	@_slot(object)
	def __new_state(self, state):
		self.update_display.emit(state['display'])
		self.update_selected_tags.emit(state['tags'])
		# TODO is there a better way to handle this derived/side-effect status ?
		self.action_status.emit(state['task'] is not None, len(state['tags']) > 0)
		self.status.emit(state['status'])

	def __connect_view(self, view):
		view.action.connect(self.__on_action)
		view.tag_button_press.connect(self.__on_tagButton_press)
		self.update_display.connect(view.on_update_display)
		self.action_status.connect(view.on_action_status)
		self.update_selected_tags.connect(view.on_update_selected_tags)
		self.status.connect(view.on_status_change)

	@_slot()
	def __reset_status(self):
		self.__store.dispatch({
			'type': 'SET_STATUS',
			'status': 'normal'
		})

		# resume the clock
		self.__clock.start()

		# refresh the display
		display = self.__store.getState()['display']
		self.update_display.emit(display)

	def __set_temporary_status(self, status, reason):
		# pause the clock
		self.__clock.stop()

		# set the status
		self.__store.dispatch({
			'type': 'SET_STATUS',
			'status': status
		})

		# send an error message
		temporary_display = _format_display_text(
			[_empty_line, _empty_line],
			0, 0,
			reason or ''
		)
		self.update_display.emit(temporary_display)

		# resume normal ops in 5 seconds...
		tmr = _Qt.QTimer(self)
		tmr.setSingleShot(True)
		tmr.setInterval(2000)
		tmr.timeout.connect(self.__reset_status)
		tmr.start()

	def __set_display_text(self, row, col, text):
		self.__store.dispatch({
			'type': 'SET_DISPLAY_TEXT',
			'row': row, 'col': col,
			'text': text
		})

	def __reset_tag_selection(self):
		self.__store.dispatch({
			'type': 'CLEAR_TAGS'
		})

	@_slot()
	def __on_clock_timeout(self):
		""" Action to perform when the clock timer times-out;
			manage the display - show current time,
			last submitted info if available,
			number entry if available
		"""

		# print('timer')
		time = _datetime.datetime.now()

		# start a new buffer for the display update
		new_text = [_empty_line, _empty_line]

		# update the clock
		new_text = _format_display_text(new_text, 1, 0, _empty_line)
		new_text = _format_display_text(new_text, 1, 11, time.strftime('%H:%M'))

		# if there is a last submission, update the
		# elapsed time and display the tags if the
		# first line is not being used for number input
		task = self.__store.getState()['task']
		if task:
			submission_time, submission_tags = task
			new_text = _format_display_text(
				new_text,
				0, 0,
				' '.join(sorted(submission_tags))
			)
			hrs, mns, _ = _timedelta_to_hhmmss(time - submission_time)
			new_text = _format_display_text(
				new_text,
				1, 0,
				'%d:%02d' % (hrs, mns)
			)
		else:
			# if there is a previous task, and not a current
			# one, then the last one must have been an 'END' ?
			# Therfore, display the idle time as a negative time
			last_task = self.__store.getState()['previousTask']
			if last_task:
				last_task_ts, _ = last_task
				idle_time = time - last_task_ts
				idle_hrs, idle_mns, _ = _timedelta_to_hhmmss(idle_time)
				new_text = _format_display_text(
					new_text,
					1, 0,
					'-%d:%02d' % (idle_hrs, idle_mns)
				)


		# push the text buffer to the display state
		self.__set_display_text(0, 0, new_text[0])
		self.__set_display_text(1, 0, new_text[1])

	@_slot()
	def __on_action(self):
		""" Handler for the "action" button; can perform
			one of many actions depending on the current state
		"""

		# any input needs to put us back in normal state
		self.__reset_status()

		# the action button can do mutliple things,
		# depending on the current state
		#
		#       | task           | !task
		# ------+----------------+-------------------
		#  tags | pending entry  | pending entry
		#       | active task    | idle
		#       | -> switch task | -> start task
		# ------+----------------+-------------------
		# !tags | no entry       | no entry
		#       | active task    | idle
		#       | -> END task    | -> no action
		# ------+----------------+-------------------
		#
		# There is no way to clear all the selected tags at once

		task = self.__store.getState()['task']
		tags = self.__store.getState().get('tags')

		if tags:
			# switch / start task
			self.submit.emit(
				_datetime.datetime.now(),
				sorted(tags)
			)
		elif task and not tags:
			# END task
			self.submit.emit(
				_datetime.datetime.now(),
				['END']
			)
		elif not task and not tags:
			# no action
			pass

	@_slot(list, bool, str)
	@_slot(list, bool)
	def __on_submit_result(self, submission, result, reason=None):
		""" Action to perform on receiving the result of
			a submission; display error, reset input,
			stash the last submission data etc.
		"""

		if not result:
			self.__set_temporary_status('error', reason)
		else:
			self.__reset_status()
			self.__reset_tag_selection()

			# if the tag was END then remove the stashed last submission,
			# else store the submission
			_, tags = submission
			if 'END' in tags:
				self.__store.dispatch({
					'type': 'CLEAR_TASK'
				})
			else:
				self.__store.dispatch({
					'type': 'SET_TASK',
					'task': submission
				})

			# this updates the last submission display
			self.__on_clock_timeout()
			self.__set_temporary_status('success', 'OK')

	@_slot(str)
	def __on_tagButton_press(self, key):
		""" Action to perform when a tag key is pressed;
			Updates the state of currently active tags
			for submission.
		"""

		# any input needs to put us back in normal state
		self.__reset_status()

		# print('tag', key)
		self.__store.dispatch({
			'type': 'TOGGLE_TAG',
			'tag': key
		})

	@_slot(bool, str)
	def on_sync_status(self, result, reason):
		if not result:
			self.__set_temporary_status('error', reason)

	@_slot(bool)
	def on_sync_active(self, is_active):
		if is_active:
			self.__store.dispatch({
				'type': 'SET_STATUS',
				'status': 'syncing'
			})
		else:
			self.__store.dispatch({
				'type': 'SET_STATUS',
				'status': 'normal'
			})