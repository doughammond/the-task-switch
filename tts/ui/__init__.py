#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

import sys as _sys
import os as _os
import signal as _signal


from PyQt5 import (
	Qt as _Qt,
	QtWidgets as _QtWidgets
)

from tts.core import (
	controller as _controller,
	models as _models,
	validation as _validation
)

from tts.net import uploader as _uploader


_settings_help = """
No configuration was found. I looked for the file %r.

The ini file should contain the following:

[local]
path = /path/to/local/data/directory

[remote]
sync = true | false
url  = https://service.example
user = user@example.com
pass = examplepassword
interval = 3600
"""

def _settings():
	settings = _Qt.QSettings(
		_Qt.QSettings.IniFormat,
		_Qt.QSettings.UserScope,
		"doughammond", "tts"
	)
	if not settings.allKeys():
		print(_settings_help % (settings.fileName(),))
		_sys.exit(-1)
	return settings

def _model(settings):
	return _models.TTSTSRModel(
		settings.value('local/path'),
		_validation.TTSAndValidator([
			_validation.TTSRequireTagsValidator(),
			_validation.TTSNotOnlyURGIMPValidator(),
			_validation.TTSSoloENDValidator()
		])
	)

def _sync(settings):
	enabled = settings.value('remote/sync') == 'true'
	interval = settings.value('remote/interval', type=int)
	# must check interval because default value is zero;
	# which would be a bad choice.
	if enabled and interval:
		# uploader is run in a thread so that the
		# synchronous network requests do not block
		# the application
		thread = _Qt.QThread()
		sync = _uploader.TTSTSRUploader(
			settings.value('local/path'),
			settings.value('remote/url'),
			settings.value('remote/user'),
			settings.value('remote/pass'),
			interval,
		)
		sync.moveToThread(thread)
		thread.start()
		return thread, sync

	return None, None

##
# signal handling
##


def sigint_handler_factory(app):
	def sigint_handler(*args):
		app.quit()
	_signal.signal(_signal.SIGINT, sigint_handler)


##
# script entry
##

def pi():
	settings = _settings()

	from tts.ui.hardware import TTSView as PiView
	app = _Qt.QCoreApplication(_sys.argv)
	sigint_handler_factory(app)
	ttsm = _model(cfg)
	ttsv = PiView()
	app.aboutToQuit.connect(ttsv.on_quit)
	ttsc = _controller.TTSController(ttsm, [ttsv])
	syncer_thread, syncer = _sync(cfg)
	if syncer:
		syncer.upload_status.connect(ttsc.on_sync_status)
		syncer.upload_active.connect(ttsc.on_sync_active)
	_sys.exit(app.exec_())


def desktop():
	settings = _settings()

	from tts.ui.desktop import TTSView as DesktopView
	app = _QtWidgets.QApplication(_sys.argv)
	ttsv = DesktopView()
	ttsm = _model(settings)
	ttsc = _controller.TTSController(ttsm, [ttsv])
	syncer_thread, syncer = _sync(settings)
	if syncer:
		syncer.upload_status.connect(ttsc.on_sync_status)
		syncer.upload_active.connect(ttsc.on_sync_active)
	ttsv.show()
	_sys.exit(app.exec_())


def both():
	settings = _settings()

	from tts.ui.desktop import TTSView as DesktopView
	from tts.ui.hardware import TTSView as PiView
	app = _QtWidgets.QApplication(_sys.argv)
	sigint_handler_factory(app)
	ttsm = _model(settings)
	ttsvd = DesktopView()
	ttsvp = PiView()
	app.aboutToQuit.connect(ttsvp.on_quit)
	ttsc = _controller.TTSController(ttsm, [ttsvp, ttsvd])
	syncer_thread, syncer = _sync(settings)
	if syncer:
		syncer.upload_status.connect(ttsc.on_sync_status)
		syncer.upload_active.connect(ttsc.on_sync_active)
	ttsvd.show()
	_sys.exit(app.exec_())
