#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

import pytest

from tts.core import validation


class NullValidator(object):
	def validate(self, a, b):
		return True, None

@pytest.fixture
def null_validator():
	return NullValidator()


def test_TTSValidator_is_abstract():
	v = validation.TTSValidator()
	with pytest.raises(NotImplementedError):
		v.validate(None, None)

def test_TTSRequireTagsValidator():
	v = validation.TTSRequireTagsValidator()

	result, reason = v.validate(None, None)
	assert result is False
	assert reason == 'Need a tag'

	result, reason = v.validate(None, [])
	assert result is False
	assert reason == 'Need a tag'

	result, reason = v.validate(None, ['Foo'])
	assert result is True
	assert reason is None

def test_TTSNotOnlyURGIMPValidator():
	v = validation.TTSNotOnlyURGIMPValidator()

	# TODO - interesting result !
	result, reason = v.validate(None, [])
	assert result is False
	assert reason == 'Need more tags'

	result, reason = v.validate(None, ['URG'])
	assert result is False
	assert reason == 'Need more tags'

	result, reason = v.validate(None, ['IMP'])
	assert result is False
	assert reason == 'Need more tags'

	result, reason = v.validate(None, ['URG', 'IMP'])
	assert result is False
	assert reason == 'Need more tags'

	result, reason = v.validate(None, ['URG', 'Foo'])
	assert result is True
	assert reason is None

	result, reason = v.validate(None, ['IMP', 'Foo'])
	assert result is True
	assert reason is None

	result, reason = v.validate(None, ['URG', 'IMP', 'Foo'])
	assert result is True
	assert reason is None

def test_TTSSoloENDValidator():
	v = validation.TTSSoloENDValidator()

	result, reason = v.validate(None, [])
	assert result is True
	assert reason is None

	result, reason = v.validate(None, ['END'])
	assert result is True
	assert reason is None

	result, reason = v.validate(None, ['END', 'Foo'])
	assert result is False
	assert reason == 'Select only END'
