#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

# pylint: disable=C0103
# - method snake-case

import os as _os

from PyQt5.QtCore import (
	pyqtSlot as _slot,
	pyqtSignal as _signal
)

from PyQt5 import (
	QtWidgets as _QtWidgets,
	uic as _uic
)


##
# constants
##

_UI_FILE = _os.path.join(
	_os.path.dirname(__file__),
	'tts.ui'
)


##
# views
##

_ALL_TAGS = set([
	'DEV',
	'SUPP',
	'PRJ',
	'EML',
	'URG',
	'SCRM',
	'MEET',
	'INTR',
	'PHN',
	'IMP'
])


class TTSView(_QtWidgets.QMainWindow):
	action = _signal()
	tag_button_press = _signal(str)

	def __init__(self, *args):
		super(TTSView, self).__init__(*args)
		_uic.loadUi(_UI_FILE, self)

	@_slot(list)
	def on_update_display(self, lines):
		text = '\n'.join(lines)
		self.lbl_Screen.setText(text)

	@_slot(str)
	def on_status_change(self, status):
		if status == 'error':
			self.lbl_Screen.setStyleSheet('QLabel { color: #CC0000; }')
		elif status == 'success':
			self.lbl_Screen.setStyleSheet('QLabel { color: #00CC00; }')
		elif status == 'syncing':
			self.lbl_Screen.setStyleSheet('QLabel { color: #0000CC; }')
		else:
			self.lbl_Screen.setStyleSheet('QLabel { color: none; }')

	@_slot(bool, bool)
	def on_action_status(self, has_task, has_tags):


		#       | task            | !task
		# ------+-----------------+-------------------
		#  tags | pending entry   | pending entry
		#       | active task     | idle
		#       | -> bright green | -> bright green
		# ------+-----------------+-------------------
		# !tags | no entry        | no entry
		#       | active task     | idle
		#       | -> dim red      | -> dim off
		# ------+-----------------+-------------------
		
		intensity = '44'
		if has_tags:
			intensity = 'DD'

		if has_task and not has_tags:
			self.lbl_Action.setStyleSheet('QLabel { background-color: #%s0000; }' % (intensity,))
		elif has_tags:
			self.lbl_Action.setStyleSheet('QLabel { background-color: #00%s00; }' % (intensity,))
		else:
			# off
			self.lbl_Action.setStyleSheet('QLabel { background-color: #000000; }')

	@_slot(set)
	def on_update_selected_tags(self, tags):
		for tag in _ALL_TAGS:
			tag_state = tag in tags
			lbl = getattr(self, 'led_%s' % (tag,), None)
			if lbl and tag_state:
				lbl.setStyleSheet('QLabel { background-color: #00CC00; }')
			elif lbl:
				lbl.setStyleSheet('QLabel { background-color: none; }')

	# UI element connections follow

	@_slot()
	def on_pb_Action_clicked(self):
		self.action.emit()

	@_slot()
	def on_pb_DEV_clicked(self):
		self.tag_button_press.emit('DEV')

	@_slot()
	def on_pb_SUPP_clicked(self):
		self.tag_button_press.emit('SUPP')

	@_slot()
	def on_pb_PRJ_clicked(self):
		self.tag_button_press.emit('PRJ')

	@_slot()
	def on_pb_EML_clicked(self):
		self.tag_button_press.emit('EML')

	@_slot()
	def on_pb_SCRM_clicked(self):
		self.tag_button_press.emit('SCRM')

	@_slot()
	def on_pb_MEET_clicked(self):
		self.tag_button_press.emit('MEET')

	@_slot()
	def on_pb_INTR_clicked(self):
		self.tag_button_press.emit('INTR')

	@_slot()
	def on_pb_PHN_clicked(self):
		self.tag_button_press.emit('PHN')

	@_slot()
	def on_pb_URG_clicked(self):
		self.tag_button_press.emit('URG')

	@_slot()
	def on_pb_IMP_clicked(self):
		self.tag_button_press.emit('IMP')
