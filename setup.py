#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

from setuptools import setup, find_packages

with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='the-task-switch',
    version='0.1.0',
    description='Working task and time recorder',
    long_description=readme,
    author='Doug Hammond',
    author_email='doug@pacifico-hammond.co.uk',
    url='https://bitbucket.com/doughammond/the-task-switch',
    license=license,
    packages=find_packages(exclude=('tests', 'docs')),
    entry_points={
        'console_scripts': [
            'tts-gui=tts.ui:desktop',
            'tts-pi=tts.ui:pi',
            'tts-both=tts.ui:both'
        ]
    }
)