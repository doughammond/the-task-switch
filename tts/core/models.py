#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

import os as _os
import datetime as _datetime

import yaml as _yaml
import yaml.loader as _yaml_loader

from PyQt5.QtCore import (
	pyqtSlot as _slot,
	pyqtSignal as _signal
)

from PyQt5 import (
	Qt as _Qt
)


def get_q_value_from_tags(tags):
	""" Convert the tag selection into a "Q"
		value; whether the tags indicate URG, IMP,
		both or neither
	"""

	if 'URG' in tags and 'IMP' in tags:
		return 'Q1'
	if 'IMP' in tags:
		return 'Q2'
	if 'URG' in tags:
		return 'Q3'
	return 'Q4'


def list_log_files(log_path):
	return [
		_os.path.join(log_path, yf)
			for yf in sorted(_os.listdir(log_path))
				if _os.path.isfile(_os.path.join(log_path, yf))
	]

def read_log_file(filename):
	log = {}

	if _os.path.exists(filename):
		with open(filename, 'r') as f_log:
			# We use the BaseLoader to prevent auto conversion
			# of the '2018-06-13' keys to datetime.date and
			# '11:34' value to some int time value.
			# This allows interoperabiliy with other implementations
			# of this app which use other yaml libraries
			log = _yaml.load(f_log, Loader=_yaml_loader.BaseLoader)

	return log

def combine_str_day_time(day, time):
	return _datetime.datetime.strptime(day + ' ' + time, '%Y-%m-%d %H:%M')


class TTSModel(_Qt.QObject):
	""" Base class implementation for the task storage model.
		This is an abstract model.
	"""

	submitResult = _signal(
		[list, bool, str],
		[list, bool]
	)

	# the model handles the constructed state
	# input which is to be persisted;
	# this is the abstract base class, sub-classes will
	# implement specific storage mechanisms,
	# e.g. text log, csv, sqlite, rethinkdb etc...

	def __init__(self, validator, *args):
		super(TTSModel, self).__init__(*args)
		self._validator = validator

	@_slot(_datetime.datetime, list, str)
	@_slot(_datetime.datetime, list)
	def on_submit(self, timestamp, tags):
		""" Action to perform on submission;
			validate (TODO not here) and pass to the store
			then emit the result in a submitResult signal
		"""

		# TODO input validation should be in the controller
		result, reason = self._validator.validate(timestamp, tags)
		if result:
			result, reason = self.store(timestamp, tags)

		self.submitResult.emit(
			[timestamp, tags],
			result, reason
		)

	def store(self, timestamp, tags):
		""" Abstract method stub for storing the submission;
			sub-classes provide specialised implementaions
		"""

		raise NotImplementedError('abstract method')

	def get_last_task(self):
		raise NotImplementedError('abstract method')


class TTSPrintModel(TTSModel):
	""" Model implementation which prints the formatted
		submission to stdout.
		This is write-only model.
	"""

	def store(self, timestamp, tags):
		""" Store the submission by printing out
			a string formatted representation
		"""

		print('%s %r %s' % (
			timestamp,
			tags,
			'-'
		))
		return True, None

	def get_last_task(self):
		# everything went to stdout, we cannot read it back
		return None


class TTSTSRModel(TTSModel):
	""" Model implementation which appends the
		submission to a YAML file using the
		timesheet-report compaible format.
		This is a read-write model.
	"""

	def __init__(self, path, *args):
		super(TTSTSRModel, self).__init__(*args)
		self._log_path = path

	def store(self, timestamp, tags):
		""" Store the submission in a timesheet-report
			compatible YAML file.
		"""

		file_name = '%s.yaml' % (
			timestamp.strftime('%Y-%m'),
		)

		try:
			if not _os.path.exists(self._log_path):
				_os.makedirs(self._log_path)

			logfile = _os.path.join(self._log_path, file_name)
			log = read_log_file(logfile)

			ts_day = timestamp.strftime('%Y-%m-%d')
			ts_time = timestamp.strftime('%H:%M')
			log_day = log.get(ts_day, [])
			log_day.append(
				[ts_time, '', get_q_value_from_tags(tags), tags]
			)
			log[ts_day] = log_day

			with open(logfile, 'w') as f_log:
				f_log.write(_yaml.dump(log))

			return True, None
		except IOError:
			return False,"IO error"
		except Exception:
			return False, "Unknown error"

	def get_last_task(self):
		try:
			try:
				lastfile = next(iter(reversed(list_log_files(self._log_path))))
			except StopIteration:
				return None

			log = read_log_file(lastfile)

			try:
				lastkey = next(iter(reversed(sorted(log.keys()))))
			except StopIteration:
				return None

			try:
				lasttask = log[lastkey][-1]
			except IndexError:
				return None

			lasttask_ts = combine_str_day_time(lastkey, lasttask[0])

			# return the start datetime and the tags
			return [lasttask_ts, lasttask[-1]]
		
		except Exception:
			return None
