#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

from PyQt5 import (
	Qt as _Qt
)


##
# validation
##

class TTSValidator(_Qt.QObject):
	""" Submitted data validator base class
	"""
	def validate(self, timestamp, tags):
		""" Entry point to validate a (timestamp, tags) tuple
			sumitted from the UI
		"""
		raise NotImplementedError('abstract method')


class TTSRequireTagsValidator(TTSValidator):
	""" Validator for rule: must have at least one tag
	"""
	def validate(self, timestamp, tags):
		if not tags:
			return False, 'Need a tag'
		return True, None


class TTSNotOnlyURGIMPValidator(TTSValidator):
	""" Validator for rule: URG/IMP cannot be selected without another tag
	"""
	def validate(self, timestamp, tags):
		urg_imp = set(['URG', 'IMP'])
		if urg_imp.issuperset(tags):
			return False, 'Need more tags'
		return True, None


class TTSCombiningValidator(TTSValidator):
	""" Validator which logically combines several other validators
	"""
	def __init__(self, child_validators, *args):
		super(TTSCombiningValidator, self).__init__(*args)
		self._child_validators = child_validators


class TTSOrValidator(TTSCombiningValidator):
	""" Combining validator for rule: OR
	"""
	def validate(self, timestamp, tags):
		result, reason = True, None
		for child in self._child_validators:
			result, reason = child.validate(timestamp, tags)
			if result:
				return True, None
		return result, reason # the last reason


class TTSAndValidator(TTSCombiningValidator):
	""" Combining validator for rule: AND
	"""
	def validate(self, timestamp, tags):
		for child in self._child_validators:
			result, reason = child.validate(timestamp, tags)
			if not result:
				return result, reason # the first reason
		return True, None


class TTSSoloENDValidator(TTSValidator):
	""" Validator for rule: END tag must appear on its own
	"""
	def validate(self, timestamp, tags):
		if len(tags) > 1 and 'END' in tags:
			return False, 'Select only END'
		return True, None

#  3. cannot have two consectutive END entries
#     -- how? this validator does not have access to the store.