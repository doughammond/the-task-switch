#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

import os as _os
import contextlib as _contextlib

from PyQt5.QtCore import (
	pyqtSlot as _slot,
	pyqtSignal as _signal
)

from PyQt5 import (
	Qt as _Qt
)

class MockWiringPi(_Qt.QObject):
	PUD_OFF = 0
	PUD_DOWN = 1
	PUD_UP = 2

	INT_EDGE_BOTH = 3

	INPUT = 1
	OUTPUT = 0

	HIGH = 1
	LOW = 0

	def __init__(self, *args):
		super(MockWiringPi, self).__init__(*args)
		self.__pin_state = {}
		self.__pin_modes = {}
		self.__i2c_registers = {}

	def digitalRead(self, pin):
		val = self.__pin_state.get(pin, 0)
		print('digitalRead', pin, val)
		return val

	def digitalWrite(self, pin, val):
		self.__pin_state[pin] = val
		print('digitalWrite', pin, val)
		return 0

	def wiringPiI2CSetup(self, *args):
		print('wiringPiI2CSetup', args)

	def mcp23017Setup(self, *args):
		print('mcp23017Setup', args)

	def wiringPiI2CWriteReg8(self, *args):
		reg = args[:-1]
		val = args[-1]
		self.__i2c_registers[reg] = val
		print('wiringPiI2CWriteReg8', reg, bin(val))

	def wiringPiI2CReadReg8(self, *args):
		val = self.__i2c_registers.get(args, None)
		print('wiringPiI2CReadReg8', args, val & 0xF)
		return val

	def wiringPiI2CWriteReg16(self, *args):
		reg = args[:-1]
		val = args[-1]
		self.__i2c_registers[reg] = val
		print('wiringPiI2CWriteReg16', reg, bin(val))

	def wiringPiI2CReadReg16(self, *args):
		val = self.__i2c_registers.get(args, None)
		print('wiringPiI2CReadReg16', args, val & 0xFF)
		return val

	def wiringPiISR(self, *args):
		print('wiringPiISR', args)

	def pinMode(self, pin, mode):
		self.__pin_modes[pin] = mode
		print('pinMode', pin, mode)

	def pullUpDnControl(self, pin, mode):
		print('pullUpDnControl', pin, mode)

	def lcdInit(self, cols, rows, mode, rs, en, b0, b1, b2, b3, b4, b5, b6, b7):
		print('lcdInit', cols, rows, mode, rs, en, b0, b1, b2, b3, b4, b5, b6, b7)

	def lcdPosition(self, h, col, row):
		print('lcdPosition', h, col, row)

	def lcdPuts(self, h, text):
		print('lcdPuts', h, text)

try:
	import wiringpi as wpi
	wpi.wiringPiSetup()
except Exception:
	wpi = MockWiringPi()



class StateScanner(_Qt.QObject):
	changed = _signal([str, int], [str, str], [str, type(None)])

	def __init__(self, interval=100, *args):
		super(StateScanner, self).__init__(*args)

		self.__states = {}

		self.__scan_timer = _Qt.QTimer(self)
		self.__scan_timer.timeout.connect(self.handle)
		self.__scan_timer.setInterval(interval)

	def start(self):
		self.__scan_timer.start()

	@_slot()
	def handle(self):
		for n, h in self.__states.items():
			v = h['fn']()

			# print(n, v)
			if v != h['state']:
				h['state'] = v
				# print(n, type(v), v)
				self.changed[str, type(v)].emit(n, v)

	def register(self, name, statefn, types, slot):
		self.__states[name] = {
			'fn': statefn,
			'state': statefn()
		}
		for t in types:
			self.changed[str, t].connect(slot)


MCP23017_OFFSET = 100

class MCP23017(_Qt.QObject):
	GPIOA = 0x12
	GPIOB = 0x13

	interrupt = _signal(int)

	def __init__(self, address, debug=False, *args):
		super(MCP23017, self).__init__(*args)
		self.__address = address

		global MCP23017_OFFSET
		self.__offset = MCP23017_OFFSET
		self.__dbg = debug

		wpi.mcp23017Setup(MCP23017_OFFSET, address)
		self.__fd = wpi.wiringPiI2CSetup(address)
		MCP23017_OFFSET += 16

	def debug(self, *args):
		if self.__dbg:
			print(args)

	def mapPinNumber(self, pin):
		return pin + self.__offset

	def pinMode(self, pin, value):
		return wpi.pinMode(self.mapPinNumber(pin), value)

	def pullUpDnControl(self, pin, mode):
		return wpi.pullUpDnControl(self.mapPinNumber(pin), mode)

	def digitalWrite(self, pin, value):
		return wpi.digitalWrite(self.mapPinNumber(pin), value)

	def digitalRead(self, pin):
		return wpi.digitalRead(self.mapPinNumber(pin))

	def setPinModes(self, mask):
		for pin in range(16):
			val = (mask & (1 << pin)) >> pin
			self.pinMode(pin, val)

	def setPullDown(self, mask):
		for pin in range(16):
			val = (mask & (1 << pin)) >> pin
			if val:
				self.pullUpDnControl(pin, wpi.PUD_DOWN)

	def setPullUp(self, mask):
		for pin in range(16):
			val = (mask & (1 << pin)) >> pin
			if val:
				self.pullUpDnControl(pin, wpi.PUD_UP)

	def setPullOff(self, mask):
		for pin in range(16):
			val = (mask & (1 << pin)) >> pin
			if val:
				self.pullUpDnControl(pin, wpi.PUD_OFF)

	def read8Bits(self, low=True):
		val = wpi.wiringPiI2CReadReg8(
			self.__fd,
			self.GPIOA if low else self.GPIOB
		)
		return val

	def read16Bits(self):
		val = wpi.wiringPiI2CReadReg16(
			self.__fd,
			self.GPIOA
		)
		return val

	def write8Bits(self, value, low=True):
		ofs = 0 if low else 8
		for pin in range(8):
			val = (value & (1 << pin)) >> pin
			self.digitalWrite(pin + ofs, val)

	def write16Bits(self, value):
		for pin in range(16):
			val = (value & (1 << pin)) >> pin
			self.digitalWrite(pin, val)

	def enableInterrupt(self, gpinten, scanner, in_pin=6):
		# fake interrupts via state scanning
		def read_16():
			return self.read16Bits() & gpinten

		statename = 'MCP23017-%s-int' % (self.__address,)

		@_slot(str, int)
		def handle(sn, v):
			if sn == statename:
				self.interrupt.emit(v)

		scanner.register(
			statename,
			read_16,
			[int], handle
		)

class RGBCharLCD_MCP23017_PinMap(object):
	def __init__(self, rs, rw, en, b0, b1, b2, b3, b4, b5, b6, b7, r, g, b):
		self.__pins = [
			rs, rw, en, b0, b1, b2, b3, b4, b5, b6, b7, r, g, b
		]

	@property
	def rs(self):
		return self.__pins[0]

	@property
	def rw(self):
		return self.__pins[1]

	@property
	def en(self):
		return self.__pins[2]

	@property
	def b0(self):
		return self.__pins[3]

	@property
	def b1(self):
		return self.__pins[4]

	@property
	def b2(self):
		return self.__pins[5]

	@property
	def b3(self):
		return self.__pins[6]

	@property
	def b4(self):
		return self.__pins[7]

	@property
	def b5(self):
		return self.__pins[8]

	@property
	def b6(self):
		return self.__pins[9]

	@property
	def b7(self):
		return self.__pins[10]

	@property
	def r(self):
		return self.__pins[11]

	@property
	def g(self):
		return self.__pins[12]

	@property
	def b(self):
		return self.__pins[13]


class RGBCharLCD(_Qt.QObject):
	def __init__(self, address, pinMap, rows=2, cols=16, dataWidth=4, *args):
		super(RGBCharLCD, self).__init__(*args)
		self.__gpio = MCP23017(address)
		self.__pinMap = pinMap
		self.__rows = rows
		self.__cols = cols

		self.__gpio.pinMode(pinMap.r, wpi.OUTPUT)
		self.__gpio.pinMode(pinMap.g, wpi.OUTPUT)
		self.__gpio.pinMode(pinMap.b, wpi.OUTPUT)
		self.__gpio.pullUpDnControl(pinMap.r, wpi.PUD_UP)
		self.__gpio.pullUpDnControl(pinMap.g, wpi.PUD_UP)
		self.__gpio.pullUpDnControl(pinMap.b, wpi.PUD_UP)

		self.__lcd = wpi.lcdInit(
			rows, cols, dataWidth,
			self.__gpio.mapPinNumber(pinMap.rs),
			self.__gpio.mapPinNumber(pinMap.en),
			self.__gpio.mapPinNumber(pinMap.b0),
			self.__gpio.mapPinNumber(pinMap.b1),
			self.__gpio.mapPinNumber(pinMap.b2),
			self.__gpio.mapPinNumber(pinMap.b3),
			self.__gpio.mapPinNumber(pinMap.b4),
			self.__gpio.mapPinNumber(pinMap.b5),
			self.__gpio.mapPinNumber(pinMap.b6),
			self.__gpio.mapPinNumber(pinMap.b7)
		)

	def setBacklight(self, r, g, b):
		self.__gpio.digitalWrite(self.__pinMap.r, not r)
		self.__gpio.digitalWrite(self.__pinMap.g, not g)
		self.__gpio.digitalWrite(self.__pinMap.b, not b)

	def setDisplay(self, display):
		wpi.lcdDisplay(self.__lcd, display)

	def setCursor(self, cursor):
		wpi.lcdCursor(self.__lcd, cursor)

	def setCursorBlink(self, blink):
		wpi.lcdCursorBlink(self.__lcd, blink)

	def setPosition(self, col, row):
		wpi.lcdPosition(self.__lcd, col, row)

	def cursorHome(self):
		wpi.lcdHome(self.__lcd)

	def clearDisplay(self):
		wpi.lcdClear(self.__lcd)

	def writeText(self, text):
		lines = text.split('\n')
		padTemplate = '%% -%ds' % (self.__cols)

		# lcdPuts will auto-wrap text across the screen, we therefore just need
		# to format each line and concatenate and write all (rows*cols) chars at once
		txt = ''
		for li, line in enumerate(lines):
			line = line + (' ' * self.__cols)
			if li > self.__rows:
				continue

			linep = padTemplate % (line[:self.__cols],)
			txt += linep

		# print('lcdPuts %r' % (txt,))
		wpi.lcdPuts(self.__lcd, str(txt))


class MAX7219(_Qt.QObject):
	# The Max7219 Registers
	NO_OP = 0x00
	DIGIT0 = 0x01
	DIGIT1 = 0x02
	DIGIT2 = 0x03
	DIGIT3 = 0x04
	DIGIT4 = 0x05
	DIGIT5 = 0x06
	DIGIT6 = 0x07
	DIGIT7 = 0x08
	DECODE_MODE = 0x09
	INTENSITY = 0X0A
	SCAN_LIMIT = 0X0B
	SHUTDOWN = 0X0C
	DISPLAY_TEST = 0X0F

	# Register Values
	MODE_NONE = 0x00
	MODE_B = 0xFF

	def __init__(self, CEO=10, MOSI=12, SCLK=14, digits=8, mode=MODE_NONE, level=0x8, *args):
		super(MAX7219, self).__init__(*args)
		# Our pins
		self.__ce0 = CEO
		self.__mosi = MOSI
		self.__sclk = SCLK

		self.__init7219(digits - 1, mode, level)

	def __send16Bits(self, output):
		mask = 0x8000
		while mask != 0:
			if output & mask:
				wpi.digitalWrite(self.__mosi, 1)
			else:
				wpi.digitalWrite(self.__mosi, 0)

			wpi.digitalWrite(self.__sclk, 1)
			mask >>= 1
			test = wpi.digitalRead(self.__sclk)
			if test != 1:
				raise Exception('SCLK failed to go high')

			wpi.digitalWrite(self.__sclk, 0)

	def __max7219Send(self, reg_number, dataout):
		wpi.digitalWrite(self.__ce0, 0)
		self.__send16Bits((reg_number << 8) + dataout)
		wpi.digitalWrite(self.__ce0, 1)
		test = wpi.digitalRead(self.__ce0)
		if test != 1:
			raise Exception('CE0 failed to go high')

	def __init7219(self, digits, mode, level):
		if not (0 <= digits <= 7):
			raise Exception('digits must be >=0 and <=7')

		# this could be extended to support mode B per digit;
		# for now it is all or none
		if mode not in (MAX7219.MODE_NONE, MAX7219.MODE_B):
			raise Exception('invalid mode')

		if not (0 <= level <= 15):
			raise Exception('level must be >=0 and <=15')

		wpi.pinMode(self.__ce0, 1)
		wpi.pinMode(self.__mosi, 1)
		wpi.pinMode(self.__sclk, 1)

		wpi.digitalWrite(self.__ce0, 1)
		wpi.digitalWrite(self.__mosi, 0)
		wpi.digitalWrite(self.__sclk, 0)

		self.__max7219Send(MAX7219.NO_OP, 0)
		self.setScanLimit(digits)
		self.setDecodeMode(mode)
		self.setDisplayTest(0)
		self.setIntensity(level)
		self.normal()

	def setDigit(self, digit, value):
		digit += 1
		all_digits = (
			MAX7219.DIGIT0, MAX7219.DIGIT1,
			MAX7219.DIGIT2, MAX7219.DIGIT3,
			MAX7219.DIGIT4, MAX7219.DIGIT5,
			MAX7219.DIGIT6, MAX7219.DIGIT7
		)
		if digit not in all_digits:
			raise Exception('digit %d is not a digit register' % (digit,))
		self.__max7219Send(digit, value)

	def setScanLimit(self, digits):
		self.__max7219Send(MAX7219.SCAN_LIMIT, digits)

	def setDecodeMode(self, mode):
		self.__max7219Send(MAX7219.DECODE_MODE, mode)

	def setDisplayTest(self, test):
		self.__max7219Send(MAX7219.DISPLAY_TEST, test)

	def setIntensity(self, level):
		self.__max7219Send(MAX7219.INTENSITY, level)

	def normal(self):
		self.__max7219Send(MAX7219.SHUTDOWN, 1)

	def shutdown(self):
		self.__max7219Send(MAX7219.SHUTDOWN, 0)
