#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

import pytest

from PyQt5.QtCore import (
	pyqtSlot as _slot
)

from tts.core import models

from test_validation import null_validator, NullValidator


class NullModel(models.TTSModel):
	def store(self, timestamp, tags):
		return True, None
	def get_last_task(self):
		return None

@pytest.fixture
def null_model():
	return NullModel(NullValidator())


def test_get_q_value_from_tags():
	q = models.get_q_value_from_tags([])
	assert q == 'Q4'

	q = models.get_q_value_from_tags(['URG'])
	assert q == 'Q3'

	q = models.get_q_value_from_tags(['IMP'])
	assert q == 'Q2'

	q = models.get_q_value_from_tags(['URG', 'IMP'])
	assert q == 'Q1'

def test_TTSModel_is_abstract(null_validator):
	submissions = []
	@_slot(list, bool, str)
	@_slot(list, bool)
	def on_submitResult(sub, result, reason=None):
		submissions.append((sub, result, reason))


	m = models.TTSModel(null_validator)
	m.submitResult.connect(on_submitResult)

	assert len(submissions) == 0

	with pytest.raises(NotImplementedError):
		m.store(1234, [])

	assert len(submissions) == 0

	with pytest.raises(NotImplementedError):
		m.on_submit(1234, [])

	assert len(submissions) == 0

def test_NullModel_on_submit_emits_submitResult(null_validator, qtbot):
	assert qtbot is not None

	submissions = []
	@_slot(list, bool, str)
	@_slot(list, bool)
	def on_submitResult(sub, result, reason=None):
		submissions.append((sub, result, reason))

	m = NullModel(null_validator)
	m.submitResult.connect(on_submitResult)

	assert len(submissions) == 0

	with qtbot.waitSignal(m.submitResult) as w:
		m.on_submit(1234, [])
	assert w.signal_triggered
	assert len(submissions) == 1

def test_TTSPrintModel(null_validator, capsys):
	m = models.TTSPrintModel(null_validator)
	m.on_submit(1234, ['FOO'])
	out, err = capsys.readouterr()
	assert out == "1234 ['FOO'] -\n"
	assert err == ''

# TODO test TTSTSRModel requires config injection and test filesystem capture
# 