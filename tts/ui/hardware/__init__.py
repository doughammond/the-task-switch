#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

# pylint: disable=C0326
# - bad whitespace rule

import os as _os
import time as _time

from PyQt5.QtCore import (
	pyqtSlot as _slot,
	pyqtSignal as _signal
)

from PyQt5 import (
	Qt as _Qt
)

from tts.ui.hardware import (
	devices as _devices
)


_TAG_LEDS = {
	'MEET': (0, 1 << 0),
	'SCRM': (0, 1 << 1),
	'URG':  (0, 1 << 2),
	'EML':  (0, 1 << 3),
	'PRJ':  (0, 1 << 4),
	'SUPP': (0, 1 << 5),
	'DEV':  (0, 1 << 6),
	'INTR': (0, 1 << 7),

	'IMP':  (1, 1 << 5),
	# 1, 1 << 3 is action "red"
	# 1, 1 << 4 is action "green"
	'PHN':  (1, 1 << 6)
}


class TTSView(_Qt.QObject):
	action = _signal()
	tag_button_press = _signal(str)

	def __init__(self, *args):
		super(TTSView, self).__init__(*args)

		self.__scanner = _devices.StateScanner()

		# Set up MCP23017 GPIO #1
		# A0 - A7 are tag buttons
		# B0 - B1 are tag buttons
		# B2 is the action button

		# We need 10 pins as inputs for the tag buttons,
		# and 1 input as the action button.
		# All with interrupt enabled.
		self.__io0 = _devices.MCP23017(0x21)
		self.__io0.interrupt.connect(self.__interrupt)
		#                             BBBBBBBBAAAAAAAA
		#                             7654321076543210
		self.__io0.setPinModes(     0b0000000000000000)

		# Not sure why, but need pauses here to ensure
		# the pullups are correctly set
		SETUP_PAUSE = 1
		_time.sleep(SETUP_PAUSE)
		self.__io0.setPullOff (     0b1111111111111111)
		_time.sleep(SETUP_PAUSE)
		self.__io0.setPullUp(       0b1111111111111111)
		_time.sleep(SETUP_PAUSE)
		self.__io0.enableInterrupt( 0b0000011111111111, self.__scanner)
		#                             7654321076543210
		#                             BBBBBBBBAAAAAAAA
		
		# Set up LED driver;
		# D0; S0 - S7 are tag button LEDS
		# D1; S0,  S6 are tag button LEDS
		# D1; S4 is action button "green"
		# D1; S3 is action button "red"
		# We need to stash the D1 state so
		# that tag LED changes do not clobber
		# the action LED states.
		# D0 state is completely tag state
		# driven and does not need to be cached.
		self.__led0_d1_state = 0b00000000
		self.__led0 = _devices.MAX7219()
		# Reset button LEDs
		self.__led0.setDigit(0, 0x00)
		self.__led0.setDigit(1, self.__led0_d1_state)

		self.__lcd = _devices.RGBCharLCD(
			0x20,
			_devices.RGBCharLCD_MCP23017_PinMap(
				# rs, rw, en, b0, b1, b2, b3, b4, b5, b6, b7, r, g, b
				0, None, 1,   2,  3,  4,  5,  9,  10, 11, 12, 6, 7, 8
			),
			dataWidth=4
		)
		self.__lcd.setBacklight(1, 1, 1)

		self.__scanner.start()

	@_slot()
	def on_quit(self):
		print('will quit, reset hardware')
		self.__io0.setPullOff(0b1111111111111111)
		self.__lcd.clearDisplay()
		self.__lcd.setBacklight(0, 0, 0)

	@_slot(int)
	def __interrupt(self, value):
		print('io0 interruptA {0:08b}'.format(value & 0xFF))
		# push buttons use negative logic
		if not (value & 0b0000000000000001):
			self.tag_button_press.emit('DEV')
		if not (value & 0b0000000000000010):
			self.tag_button_press.emit('SUPP')
		if not (value & 0b0000000000000100):
			self.tag_button_press.emit('PRJ')
		if not (value & 0b0000000000001000):
			self.tag_button_press.emit('EML')
		if not (value & 0b0000000000010000):
			self.tag_button_press.emit('URG')
		if not (value & 0b0000000000100000):
			self.tag_button_press.emit('SCRM')
		if not (value & 0b0000000001000000):
			self.tag_button_press.emit('MEET')
		if not (value & 0b0000000010000000):
			self.tag_button_press.emit('INTR')

		print('io0 interruptB {0:08b}'.format(value >> 8))
		if not (value & 0b0000000100000000):
			self.tag_button_press.emit('PHN')
		if not (value & 0b0000001000000000):
			self.tag_button_press.emit('IMP')

		# B2 is the action button
		if not (value & 0b0000010000000000):
			self.action.emit()

	@_slot(list)
	def on_update_display(self, lines):
		# print('LCD: ', ' - '.join(lines).strip())
		self.__lcd.writeText('\n'.join(lines))

	@_slot(str)
	def on_status_change(self, status):
		# print('LCD status change:', status)
		if status == 'error':
			self.__lcd.setBacklight(1, 0, 0)
		elif status == 'success':
			self.__lcd.setBacklight(0, 1, 0)
		elif status == 'syncing':
			self.__lcd.setBacklight(0, 0, 1)
		else:
			self.__lcd.setBacklight(1, 1, 1)

	@_slot(bool, bool)
	def on_action_status(self, has_task, has_tags):
		# Use the D1 state as a start
		state = self.__led0_d1_state

		#       | task            | !task
		# ------+-----------------+-------------------
		#  tags | pending entry   | pending entry
		#       | active task     | idle
		#       | -> bright green | -> bright green
		# ------+-----------------+-------------------
		# !tags | no entry        | no entry
		#       | active task     | idle
		#       | -> dim red      | -> dim off
		# ------+-----------------+-------------------

		intensity = 10
		if has_tags:
			intensity = 15

		if has_task and not has_tags:
			# D1 S4 is "red"
			state |= 1 << 3
		elif has_tags:
			# D1 S5 is "green"
			state |= 1 << 4
		else:
			# off
			state &= 0xFF - (1 << 3 | 1 << 4)

		if state != self.__led0_d1_state:
			self.__led0_d1_state = state
			self.__led0.setDigit(1, state)
			self.__led0.setIntensity(intensity)

	@_slot(set)
	def on_update_selected_tags(self, tags):
		digits = [
			0x0,
			# Use the D1 state as a start, so that
			# we maintain the action LEDs state
			self.__led0_d1_state
		]
		for tag, (digit, segment) in _TAG_LEDS.items():
			tag_state = tag in tags
			if tag_state:
				digits[digit] |= segment
			else:
				digits[digit] &= 0xFF - segment

		self.__led0_d1_state = digits[1]

		for digit, state in enumerate(digits):
			self.__led0.setDigit(digit, state)