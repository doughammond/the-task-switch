#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

import requests as _requests


from PyQt5.QtCore import (
	pyqtSlot as _slot,
	pyqtSignal as _signal
)

from PyQt5 import (
	Qt as _Qt
)

from tts.core import models as _models

class StopUploading(Exception):
	pass


class TTSTSRUploader(_Qt.QObject):
	upload_active = _signal(bool)
	upload_status = _signal(bool, str)

	def __init__(self, log_path, url, auth_user, auth_pass, interval_secs, *args):
		super(TTSTSRUploader, self).__init__(*args)
		self._log_path = log_path
		self._auth = (auth_user, auth_pass)
		self._url = '%s/api/v1' % (url,)

		self._last_upload_item = None
		self._is_uploading = False

		self._tmr = _Qt.QTimer()
		self._tmr.timeout.connect(self.upload_yaml_files)
		self._tmr.setInterval(1000 * interval_secs)
		self._tmr.start()

	def _route(self, *path):
		return '%s/%s' % (self._url, '/'.join(path))

	def _get(self, *path):
		return _requests.get(
			self._route(*path),
			auth=self._auth
		)

	def _put(self, *path, **kw):
		return _requests.put(
			self._route(*path),
			auth=self._auth,
			**kw
		)

	def _store_item(self, timestamp, tags):
		""" attempt to store an item to the API if needed;
			this method has a tri-state return:
				None - no action was attempted
				False - API call failed
				True - API call successfull
		"""

		if self._last_upload_item is not None:
			# if this item happened before last synced item, then skip
			if timestamp < self._last_upload_item[0]:
				return None, None
			# else if this item has the same timestamp and tags, then skip
			elif timestamp == self._last_upload_item[0] and tags == self._last_upload_item[1]:
				return None, None
			# else it can be uploaded
			
			# TODO there's still a logical flaw here, consider:
			#  - 2018-06-09 14:01 ['PRJ']
			#  - 2018-06-09 14:01 ['END']
			# 
			# Both have already been uploaded, and the last item is ['END']
			# Next time around, ['PRJ'] is different from the last item upload
			# and is therefore uploaded again. The same happens for ['END'].
			# Therefore, of there are multiple items at the end of the timesheet
			# within the same minute, they will all be repeatedly uploaded :(
			# The API is tolerant of this, except that the timesheet entry
			# 'updated' property will be constantly refreshed to the sync time.

		try:
			serialised_item = {
				'time_started': timestamp.isoformat(),
				'categories': list(tags)
			}
			response = self._put(
				'timesheet_item',
				json=serialised_item
			)

			success = response.status_code == _requests.codes.ok
			if success:
				self._last_upload_item = (timestamp, tags)

			return success, 'API status: %s' % (response.status_code,)
		except IOError as err:
			reason = 'API error:\n%s' % (type(err).__name__,)
			print(reason, err)
			return False, reason

	@_slot()
	def upload_yaml_files(self):
		# prevent re-entry if the timer fires before we're finished
		if self._is_uploading:
			return

		self._is_uploading = True
		self.upload_active.emit(True)
		upload_result = None
		last_reason = None

		try:
			for yf in _models.list_log_files(self._log_path):
				timesheet = _models.read_log_file(yf)
				for day, items in sorted(timesheet.items()):
					for item in items:
						item_ts = _models.combine_str_day_time(day, item[0])
						item_tg = item[-1]
						result, reason = self._store_item(item_ts, item_tg)

						if result is True:
							print('Uploader : %s %s OK' % (item_ts, item_tg))
							upload_result = True

						# we're going to stop at the first error,
						# because it is likely network related
						if result is False:
							upload_result = False
							last_reason = reason
							raise StopUploading()

		except StopUploading:
			pass

		self.upload_active.emit(False)
		if upload_result is not None:
			self.upload_status.emit(upload_result, last_reason)
		
		self._is_uploading = False