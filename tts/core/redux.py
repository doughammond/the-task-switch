#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

from PyQt5.QtCore import (
	pyqtSignal as _signal
)

from PyQt5 import (
	Qt as _Qt
)


def LoggerMiddleware(store):
	def _n(next_dispatch):
		def _dispatch(action):
			print(action)
			next_dispatch(action)
		return _dispatch
	return _n


def SagaMiddleware(sagas):
	def _m(store):
		def _n(next_dispatch):
			def _dispatch(action):
				for saga in sagas:
					for action_type, task in saga():
						if action_type == action['type']:
							for side_action in task(action, store):
								next_dispatch(side_action)
				next_dispatch(action)
			return _dispatch
		return _n
	return _m

# Saga example
# def _testSagaTask1(action, store):
# 	if action['tag'] == 'EML':
# 		yield { 'type': 'S_PUT' }

# def testSaga():
# 	yield 'TOGGLE_TAG', _testSagaTask1



class Store(_Qt.QObject):
	newstate = _signal(object)

	def __init__(self, reducer, *args):
		super(Store, self).__init__(*args)
		self.__reducer = reducer
		self.__state = None

		self.__dispatcher = lambda action: self.__single_dispatch(action)

		# ensure we have a default state
		self.dispatch({ 'type': None })

	def apply_middleware(self, middlewares):
		d = self.__dispatcher
		for m in reversed(middlewares):
			d = m(self)(d)
		self.__dispatcher = d

	@property
	def dispatch(self):
		return self.__dispatcher

	def __single_dispatch(self, action):
		self.__state = self.__reducer(action, self.__state)
		self.newstate.emit(self.__state)

	def subscribe(self, slot):
		self.newstate.connect(slot)

	def getState(self):
		return self.__state