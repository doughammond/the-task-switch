#
# SPDX-License-Identifier: GPL-3.0-only
#
# Copyright (C) 2018 Doug Hammond
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

import pytest
import mock
import freezegun
import datetime

from tts.core import controller


# Utilities

def test__format_display_text():
	buf = [
		' ' * 16,
		' ' * 16
	]

	res = controller._format_display_text(
		buf,
		0, 0,
		'foo'
	)

	# the same input object is returned, modified
	assert buf is res
	assert buf == [
		'foo             ',
		'                '
	]

	# formatting does not erase previous data
	res = controller._format_display_text(
		buf,
		0, 13,
		'bar'
	)
	assert buf == [
		'foo          bar',
		'                '
	]

	# formatting does not erase previous data
	res = controller._format_display_text(
		buf,
		0, 6,
		'baz'
	)
	assert buf == [
		'foo   baz    bar',
		'                '
	]

	# attempting to format out of bounds has no effect
	res = controller._format_display_text(
		buf,
		0, 16,
		'quxx'
	)
	assert buf == [
		'foo   baz    bar',
		'                '
	]

	# attempting to format out of bounds has no effect
	res = controller._format_display_text(
		buf,
		2, 0,
		'quxx'
	)
	assert buf == [
		'foo   baz    bar',
		'                '
	]

	# exceeding the buffer line length truncates the input
	res = controller._format_display_text(
		buf,
		0, 13,
		'doug'
	)
	assert buf == [
		'foo   baz    dou',
		'                '
	]

def test__timedelta_to_hhmmss():
	import datetime
	seconds = 3 * 3600 + 25 * 60 + 45
	td = datetime.timedelta(seconds=seconds)
	hhmmss = controller._timedelta_to_hhmmss(td)
	assert hhmmss == (3, 25, 45.0)



# Application reducers

def test_displayReducer_SET_DISPLAY_TEXT():
	state = controller.displayReducer({
		'type': 'SET_DISPLAY_TEXT',
		'row': 0, 'col': 0,
		'text': 'foo'
	})
	assert state == [
		'foo             ',
		'                '
	]

def test_tagsReducer_CLEAR_TAGS():
	state = controller.tagsReducer(
		{ 'type': 'CLEAR_TAGS' },
		set(['FOO', 'BAR'])
	)
	assert state == set()

def test_tagsReducer_TOGGLE_TAG():
	state = controller.tagsReducer(
		{
			'type': 'TOGGLE_TAG',
			'tag': 'FOO'
		}
	)
	assert state == set(['FOO'])

	state = controller.tagsReducer(
		{
			'type': 'TOGGLE_TAG',
			'tag': 'FOO'
		},
		state)
	assert state == set()

def test_statusReducer_SET_STATUS():
	state = controller.statusReducer({
		'type': 'SET_STATUS',
		'status': 'foo'
	})
	assert state == 'foo'

def test_taskReducer_CLEAR_TASK():
	state = controller.taskReducer(
		{
			'type': 'CLEAR_TASK'
		},
		(1234, [])
	)
	assert state is None

def test_taskReducer_SET_TASK():
	state = controller.taskReducer(
		{
			'type': 'SET_TASK',
			'task': (1234, [])
	})
	assert state == (1234, [])

def test_appReducer():
	# assert a None action returns the default state
	state = controller.appReducer({ 'type': None })
	assert state == {
		'display': [
			'                ',
			'                '
		],
		'status': None,
		'tags': set(),
		'task': None,
		'previousTask': None
	}


# Application controller

from test_models import null_model
from test_views import null_view

# the controller uses the current time, so we will freeze that
_TEST_TIME = datetime.datetime(2018, 4, 16, 13, 24)

@pytest.fixture
@freezegun.freeze_time(_TEST_TIME)
def mvcs(null_model, null_view):
	""" Model, View, Controller, Store fixture
	"""
	c = controller.TTSController(null_model, [null_view])

	# break out the state so that we can inspect it
	s = c._TTSController__store

	return null_model, null_view, c, s

@freezegun.freeze_time(_TEST_TIME)
def test_TTSController_initial_state(mvcs):
	# test the initial state
	m, v, c, s = mvcs

	assert s.getState() == {
		'display': [
			'                ',
			'           13:24'
		],
		'status': None,
		'tags': set(),
		'task': None,
		'previousTask': None
	}

@freezegun.freeze_time(_TEST_TIME)
def test_TTSController_tag_input(mvcs):
	m, v, c, s = mvcs

	# toggle a tag on
	v.tag_button_press.emit('FOO')
	assert s.getState() == {
		'display': [
			'                ',
			'           13:24'
		],
		'status': 'normal',
		'tags': set(['FOO']),
		'task': None,
		'previousTask': None
	}

	# toggle a tag off
	v.tag_button_press.emit('FOO')
	assert s.getState() == {
		'display': [
			'                ',
			'           13:24'
		],
		'status': 'normal',
		'tags': set(),
		'task': None,
		'previousTask': None
	}

@freezegun.freeze_time(_TEST_TIME)
def test_TTSController_action_not_task_not_tags(mvcs):
	m, v, c, s = mvcs

	# no state setup;
	# - no entry
	# - idle

	# -> no action
	# no major change of state,
	# just 'status' got reset
	v.action.emit()
	assert s.getState() == {
		'display': [
			'                ',
			'           13:24'
		],
		'status': 'normal',
		'tags': set(),
		'task': None,
		'previousTask': None
	}

@freezegun.freeze_time(_TEST_TIME)
def test_TTSController_action_not_task_tags(mvcs):
	m, v, c, s = mvcs

	# set up the state;
	# - pending entry
	# - idle
	s.dispatch({
		'type': 'TOGGLE_TAG',
		'tag': 'FOO'
	})

	# -> start task
	v.action.emit()
	assert s.getState() == {
		'display': [
			'FOO             ',
			'0:00       13:24'
		],
		'status': 'success',
		'tags': set(),
		'task': [_TEST_TIME, ['FOO']],
		'previousTask': None
	}

@freezegun.freeze_time(_TEST_TIME)
def test_TTSController_action_task_tags(mvcs):
	m, v, c, s = mvcs

	# set up the state;
	# - pending entry
	# - active task
	s.dispatch({
		'type': 'TOGGLE_TAG',
		'tag': 'BAR'
	})
	s.dispatch({
		'type': 'SET_TASK',
		'task': [_TEST_TIME, ['FOO']]
	})

	# -> swicth task
	v.action.emit()
	assert s.getState() == {
		'display': [
			'BAR             ',
			'0:00       13:24'
		],
		'status': 'success',
		'tags': set(),
		# in reality task and previousTask
		# would not necessarily have the same
		# time stamp
		'task': [_TEST_TIME, ['BAR']],
		'previousTask': [_TEST_TIME, ['FOO']]
	}

@freezegun.freeze_time(_TEST_TIME)
def test_TTSController_action_task_not_tags(mvcs):
	m, v, c, s = mvcs

	# set up the state;
	# - no entry
	# - active task
	s.dispatch({
		'type': 'SET_TASK',
		'task': [_TEST_TIME, ['FOO']]
	})

	# -> END task
	v.action.emit()
	assert s.getState() == {
		'display': [
			'                ',
			'-0:00      13:24'
		],
		'status': 'success',
		'tags': set(),
		'task': None,
		# the END time just happens to be
		# the same as _TEST_TIME because
		# time is fozen; in reality this
		# would not be the same value!
		'previousTask': [_TEST_TIME, ['END']]
	}


# TODO test temporary display

# TODO test with validation error

# TODO test END tag submit
