#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

import mock
import pytest

from PyQt5 import (
	Qt as _Qt
)

from PyQt5.QtCore import (
	pyqtSlot as _slot,
	pyqtSignal as _signal
)


class NullView(_Qt.QObject):
	action = _signal()
	tag_button_press = _signal(str)

	def __init__(self, *args):
		super(NullView, self).__init__(*args)

		self.mock_display = mock.Mock()
		self.mock_status = mock.Mock()
		self.mock_action = mock.Mock()
		self.mock_tags = mock.Mock()

	@_slot(list)
	def on_update_display(self, lines):
		self.mock_display.on_update_display(lines)

	@_slot(str)
	def on_status_change(self, status):
		self.mock_status.on_status_change(status)

	@_slot(bool, bool)
	def on_action_status(self, has_task, has_tags):
		self.mock_action.on_action_status(has_task, has_tags)

	@_slot(set)
	def on_update_selected_tags(self, tags):
		self.mock_tags.on_update_selected_tags(tags)

@pytest.fixture
def null_view():
	return NullView()
