#
# SPDX-License-Identifier: GPL-3.0-only
# 
# Copyright (C) 2018 Doug Hammond
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
# 

import mock

from PyQt5.QtCore import (
	pyqtSlot as _slot
)

from tts.core import redux


def test_Store(qtbot):
	statemock = mock.Mock()
	@_slot()
	def state_receiver(newstate):
		statemock(newstate)

	def test_reducer(action, state=None):
		state = state or {}
		if action['type'] == 'RUN_TEST':
			state['result'] = action['test']
		return state

	ss = redux.Store(test_reducer)
	ss.subscribe(state_receiver)

	# default state is initialised
	assert ss.getState() == {}

	# unrecognised action has no effect on the state
	with qtbot.waitSignal(ss.newstate) as w:
		ss.dispatch({
			'type': 'UNRECOGNISED_ACTION',
		})

	statemock.assert_called_with({})
	assert ss.getState() == {}

	# recognised action updates the state
	with qtbot.waitSignal(ss.newstate) as w:
		ss.dispatch({
			'type': 'RUN_TEST',
			'test': 'foo'
		})

	statemock.assert_called_with({
		'result': 'foo'
	})
	assert ss.getState() == {
		'result': 'foo'
	}


def test_Store_middleware(qtbot):
	loggermock = mock.Mock()
	def logger_mw(store):
		def _n(next):
			def _r(action):
				loggermock(action)
				return next(action)
			return _r
		return _n

	statemock = mock.Mock()
	@_slot()
	def state_receiver(newstate):
		statemock(newstate)

	def test_reducer(action, state=None):
		state = state or {}
		if action['type'] == 'RUN_TEST':
			state['result'] = action['test']
		return state

	ss = redux.Store(
		test_reducer
	)
	ss.apply_middleware([logger_mw])
	ss.subscribe(state_receiver)

	# default state is initialised
	assert ss.getState() == {}

	# unrecognised action has no effect on the state
	with qtbot.waitSignal(ss.newstate) as w:
		ss.dispatch({
			'type': 'UNRECOGNISED_ACTION',
		})

	statemock.assert_called_with({})
	assert ss.getState() == {}

	# recognised action updates the state
	with qtbot.waitSignal(ss.newstate) as w:
		ss.dispatch({
			'type': 'RUN_TEST',
			'test': 'foo'
		})

	statemock.assert_called_with({
		'result': 'foo'
	})
	assert ss.getState() == {
		'result': 'foo'
	}

	assert loggermock.call_count == 2
	loggermock.assert_has_calls([
		mock.call({ 'type': 'UNRECOGNISED_ACTION' }),
		mock.call({ 'type': 'RUN_TEST', 'test': 'foo' })
	])
